#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import json
import unittest
import math

from cklabidx.analysis.analyzer import Analyzer
from cklabidx.analysis.tokenizers.whitespace_tokenizer import WhitespaceTokenizer
from cklabidx.analysis.filters.lower_case_filter import LowerCaseFilter
from cklabidx.analysis.processors.en_punctuation_processor import PunctuationProcessor as en_pp
from cklabidx.index.ram_index import RAMInvertedTableIndex

from cklabidx.query.query_parser import QueryParser
from cklabidx.search.searcher import DocumentSearcher
from cklabidx.search.similarity.tfidf_similarity import TFIDFSimilarity

# TODO: test TF and IDF functions


class RAMIndexTest(unittest.TestCase):

    def setUp(self):
        self.analyzer = Analyzer()
        self.analyzer.set_default(WhitespaceTokenizer(), [LowerCaseFilter()], [en_pp])

        index_obj = RAMInvertedTableIndex()
        index_obj.set_analyzer(self.analyzer)

        self.index_obj = index_obj
        self.input_path = "sample/easy/easy.json"
        self.index_path = "tmp/tests/"
        self.index_name = "test_ram_index_easy"

    def tearDown(self):
        self.index_obj = None

    def test_add_save_load_documents(self):

        # Test: Add documents
        with open(self.input_path) as data_file:
            data = json.load(data_file)
            for doc_id, doc_dict in data.items():
                # Add document to Index Object
                self.index_obj.add_document(doc_id, doc_dict)

        self.assertEqual(self.index_obj.get_unique_doc_counts(), 3)
        self.assertEqual(self.index_obj.get_unique_term_counts(), 16)
        self.assertEqual(self.index_obj.get_unique_field_counts(), 2)

        # Test: Save / Load index
        self.index_obj.save(self.index_path, self.index_name)
        self.index_obj.load(self.index_path, self.index_name)

        self.assertEqual(self.index_obj.get_unique_doc_counts(), 3)
        self.assertEqual(self.index_obj.get_unique_term_counts(), 16)
        self.assertEqual(self.index_obj.get_unique_field_counts(), 2)

        # Test: Save / Load index (JSON)
        self.index_obj.save_json(self.index_path, self.index_name)
        self.index_obj.load_json(self.index_path, self.index_name)

        self.assertEqual(self.index_obj.get_unique_doc_counts(), 3)
        self.assertEqual(self.index_obj.get_unique_term_counts(), 16)
        self.assertEqual(self.index_obj.get_unique_field_counts(), 2)

        # Test: TF function / IDF function
        self.assertEqual(self.index_obj.get_term_tf("easy01","content","aa"), 0.2)
        self.assertEqual(self.index_obj.get_term_tf("easy02","content","aa"), 0.0)
        self.assertEqual(self.index_obj.get_term_idf_classic("content","aa"), math.log10( 3 / 1 ))
        self.assertEqual(self.index_obj.get_term_idf_classic("content","ee"), math.log10( 3 / 2 ))
        self.assertEqual(self.index_obj.get_term_idf_lucene_default("content","aa"), math.log10( 3 / (1 + 1) ) + 1)
        self.assertEqual(self.index_obj.get_term_idf_lucene_default("content","ee"), math.log10( 3 / (2 + 1) ) + 1)
        

    def test_search_documents(self):

        # Test: Add documents
        with open(self.input_path) as data_file:
            data = json.load(data_file)
            for doc_id, doc_dict in data.items():
                # Add document to Index Object
                self.index_obj.add_document(doc_id, doc_dict)

        self.assertEqual(self.index_obj.get_unique_doc_counts(), 3)
        self.assertEqual(self.index_obj.get_unique_term_counts(), 16)
        self.assertEqual(self.index_obj.get_unique_field_counts(), 2)

        # Test: Search
        # query = QueryParser(self.analyzer)
        # query.add_field("title", "easy")
        # query.add_field("content", "AA Bb")
        #
        # scorer = TFIDFSimilarity(use_explain=True)
        # searcher = DocumentSearcher(self.index_obj, scorer)
        #
        # doc_collector = searcher.search(query)
        #
        # print(query)
        # print("Search results:")
        # print(doc_collector.get_field_top_documents("content", 0, 10))
        # print(doc_collector.get_overall_top_documents(0, 10))
        #
        # print("Scoring Formula:")
        # print(scorer.explain)


if __name__ == '__main__':
    unittest.main()
