#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#
# TODO: modify stemming_filter test file (ok
import unittest
from cklabidx.analysis.filters.stemming_filter import StemmingFilter


class StemmingFilterTest(unittest.TestCase):

    def test_stemming_filter(self):

        self.filter = StemmingFilter()

        self.assertEqual(self.filter.filter(["dog", "dogs"]), ["dog", "dog"])
        self.assertEqual(self.filter.filter(["church", "churches"]), ["church", "church"])
        self.assertEqual(self.filter.filter(["leaf", "leaves"]), ["leaf", "leaf"])
        self.assertEqual(self.filter.filter(["country", "countries"]), ["country", "country"])
        self.assertEqual(self.filter.filter(["work", "works", "worked", "working"]), ["work", "work", "work", "work"])
        self.assertEqual(self.filter.filter(["go", "goes", "went", "gone", "going"]), ["go", "go", "go", "go", "go"])

if __name__ == '__main__':
    unittest.main()
