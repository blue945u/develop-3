#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#
# TODO: modify zh_punctuation_processor test file

import unittest
from cklabidx.analysis.processors.zh_punctuation_processor import PunctuationProcessor


class PunctuationProcessorTest(unittest.TestCase):

    def test_punctuation_processor(self):

        self.zhpp = PunctuationProcessor()

        self.assertEqual(self.zhpp.process("「日本的童書，很多故事沒有結局。」"), "日本的童書很多故事沒有結局")
        self.assertEqual(self.zhpp.process("《格雷的五十道陰影》"), "格雷的五十道陰影")
        self.assertEqual(self.zhpp.process("三個目標："), "三個目標")
        self.assertEqual(self.zhpp.process("一、藍軍執政縣市要增加。"), "一藍軍執政縣市要增加")
        self.assertEqual(self.zhpp.process("二、六都最少要拿二。"), "二六都最少要拿二")
        self.assertEqual(self.zhpp.process("三、國民黨全國得票數要增加。"), "三國民黨全國得票數要增加")
        self.assertEqual(self.zhpp.process("！，。？、「」『』；（）："), "")


if __name__ == '__main__':
    unittest.main()