#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import math
import os

from cklabidx.index.base_index import InvertedTableIndex
from cklabidx.index.data.inverted_table import DocValues, TermValues
from cklabidx.index.data.index_stats import DocStats, FieldStats


class RAMInvertedTableIndex(InvertedTableIndex):
    """
    Main Index object
    """

    def __init__(self):

        # Initialize base index with Analyzer()
        super().__init__()

        # Inverted table data structures
        self.doc_values = None
        self.term_values = None

        # Index stats data structures
        self.doc_stats = None
        self.field_stats = None

        # Initialize Data
        self._initialize_memory()

    def _initialize_memory(self):
        # Inverted table data structures
        self.doc_values = DocValues()
        self.term_values = TermValues()

        # Index stats data structures
        self.doc_stats = DocStats()
        self.field_stats = FieldStats()

    def add_document(self, doc_id, content_dict):

        for field, content in content_dict.items():

            # Apply preprocessor, tokenizer, filter
            tokens = self.analyzer.analyze(field, content)

            # Add document to inverted table
            self.doc_values.add_document(doc_id, field, tokens)
            self.term_values.add_document(doc_id, field, tokens)

            # Update index stats
            self.doc_stats.add_document(doc_id, field, tokens)
            self.field_stats.add_document(doc_id, field, tokens)

    def del_document(self, doc_id):
        raise NotImplementedError

    def update_document(self, doc_id, content_dict):
        raise NotImplementedError

    def save(self, path, index_name):
        """
        Use Pickle dumps to store index files
        :param path:
        :param index_name:
        :return:
        """
        save_path = os.path.join(path, index_name)

        # Create subdir(index_name) in given path
        if not os.path.exists(save_path):
            os.makedirs(save_path, 0o755)

        # Inverted table
        self.doc_values.dump_pickle(save_path)
        self.term_values.dump_pickle(save_path)

        # Index stats
        self.doc_stats.dump_pickle(save_path)
        self.field_stats.dump_pickle(save_path)

    def save_json(self, path, index_name):
        """
        Use JSON dumps to store index files
        :param path:
        :param index_name:
        :return:
        """
        save_path = os.path.join(path, index_name)

        # Inverted table
        self.doc_values.dump_json(save_path)
        self.term_values.dump_json(save_path)

        # Index stats
        self.doc_stats.dump_json(save_path)
        self.field_stats.dump_json(save_path)

    def load(self, path, index_name):
        """
        Use Pickle loads to load index files
        :param path:
        :param index_name:
        :return:
        """

        # Inverted table
        self.doc_values.load_pickle(os.path.join(path, index_name))
        self.term_values.load_pickle(os.path.join(path, index_name))

        # Index stats
        self.doc_stats.load_pickle(os.path.join(path, index_name))
        self.field_stats.load_pickle(os.path.join(path, index_name))

    def load_json(self, path, index_name):
        """
        Use JSON loads to load index files
        :param path:
        :param index_name:
        :return:
        """

        # Inverted table
        self.doc_values.load_json(os.path.join(path, index_name))
        self.term_values.load_json(os.path.join(path, index_name))

        # Index stats
        self.doc_stats.load_json(os.path.join(path, index_name))
        self.field_stats.load_json(os.path.join(path, index_name))

    def get_term_tf(self, doc, field, term):
        """
        Return Term Frequency

            for term(i), document(j), term(1~k) in document(j):

            tf(i,j) = n(i,j) / SUM(n(k,j))

        (Ref: https://en.wikipedia.org/wiki/Tf%E2%80%93idf)

        :param doc: document id
        :param field: field name
        :param term: term
        :return: term frequency
        """

        # TODO: Add TF function
        n = self.get_term_counts(doc, field, term)
        sum_n = self.get_total_term_counts(doc, field)
        tf = n/sum_n # Dummy value
        return tf

    def get_term_counts(self, doc, field, term):
        """
        Return term counts of a specific term in doc/filed
        :param doc:
        :param field:
        :param term:
        :return:
        """
        if doc in self.doc_values.data_dict:
            if field in self.doc_values.data_dict[doc]:
                if term in self.doc_values.data_dict[doc][field]:
                    return self.doc_values.data_dict[doc][field][term]
        # Else:
        return 0

    def get_sum_total_term_counts(self, field):
        """
        Returns the total number of tokens for certain field
        :param field:
        :return:
        """
        sum_total_term_counts = self.doc_stats.get_stats_by_field(field)

        if sum_total_term_counts <= 0:
            return 1
        else:
            return sum_total_term_counts

    def get_total_term_counts(self, doc, field):
        """
        Return total term counts in specified document field
        :param doc:
        :param field:
        :return:
        """
        return self.doc_stats.get_stats_by_doc(field, doc)

    def get_term_counts_in_all_documents(self, field, term):
        """
        Return term counts of a specific term in filed of all documents
        :param field:
        :param term:
        :return:
        """
        if field in self.term_values.terms and term in self.term_values.terms[field]:
            sum_n = 0
            for counts in self.term_values.terms[field][term].values():
                sum_n += counts
            return sum_n
        else:
            return 0

    def get_doc_counts_contains_term(self, field, term):
        """
        Return total document counts contains term(i) (td)
        :param field:
        :param term:
        :return:
        """
        if field in self.term_values.data_dict and term in self.term_values.data_dict[field]:
            return len(self.term_values.data_dict[field][term])
        else:
            return 0

    def get_term_idf(self, field, term):
        """
        Return Inverted Document Frequency of a term, return Lucene version by default.
        """
        return self.get_term_idf_lucene_default(field, term)

    def get_term_idf_classic(self, field, term):
        """
        Return Inverted Document Frequency of a term, using classic definition.

            for term(i),
                total document counts(d),
                total document counts contains term(i) (td):

            idf(i) = log( d / td )

        (Ref: https://en.wikipedia.org/wiki/Tf%E2%80%93idf)

        :param field: field
        :param term: term
        :return: inverted document frequency
        """

        # TODO: Add IDF function
        d = self.get_unique_doc_counts()
        td = self.get_doc_counts_contains_term(field, term)
        idf = math.log10( d / td ) # Dummy value
        return idf

    def get_term_idf_lucene_default(self, field, term):
        """
        Return Inverted Document Frequency of a term, using Lucene default version.

            for term(i),
                total document counts(d),
                total document counts contains term(i) (td):

            idf(i) = log( d / (td + 1) ) + 1

        (Ref: https://lucene.apache.org/core/6_0_0/core/org/apache/lucene/search/similarities/TFIDFSimilarity.html)

        :param field: field
        :param term: term
        :return: inverted document frequency
        """
        
        # TODO: Add IDF function
        d = self.get_unique_doc_counts()
        td = self.get_doc_counts_contains_term(field, term)
        idf = math.log10( d / (td + 1) ) + 1  # Dummy value
        return idf

    def get_length_norm_of_doc(self, doc, field):
        """
        Return index-time normalization value of a document field.

        Implemented as:

                (float) (1.0 / Math.sqrt( total term counts in a document field)

        :param doc:
        :param field:
        :return:
        """
        sum_terms = self.get_total_term_counts(doc, field)
        length_norm = 1 / math.sqrt(sum_terms)
        return length_norm

    def get_unique_doc_counts(self):
        """
        Return total document counts(d)

        :return:
        """
        return self.doc_values.get_document_counts()

    def get_unique_field_counts(self):
        """
        Return total field counts(f)

        :return:
        """
        return self.term_values.get_field_counts()

    def get_unique_term_counts(self):
        """
        Return total terms counts(t) in all fields

        :return:
        """
        terms = set()
        for field in self.term_values.data_dict:
            terms.update(self.term_values.data_dict[field].keys())

        return len(terms)

    def get_doc_term_vector(self, doc, field):
        """
        Return document in the form of bags-of-words vector

        :param doc:
        :param field:
        :return:
        """
        return self.doc_values.get_doc_term_vector(doc, field)

    def search(self, field, term):
        """
        Implement boolean model. Return a list of doc_id which contains the specified term.

        :param field:
        :param term:
        :return: list of doc_id
        """
        if field in self.term_values.data_dict and term in self.term_values.data_dict[field]:
            return list(self.term_values.data_dict[field][term].keys())
        else:
            return []
