#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

from cklabidx.index.data.base_index_data import BaseIndexData


class SuffixArray(BaseIndexData):
    """
    Data structure for Suffix Array Index.
    """
    def __init__(self):
        super().__init__()

        # Override
        self.data_dict = {}
        self.file_name_json = ""
        self.file_name_pickle = ""

    def add_document(self, doc_id, field, term_list):
        pass

    # TODO: Add something (no need to do it in project 1


class MainText(BaseIndexData):
    """
    Data structure for Suffix Array Data.
    """

    def __init__(self):
        super().__init__()

        # Override
        self.data_dict = {}
        self.file_name_json = ""
        self.file_name_pickle = ""

    def add_document(self, doc_id, field, term_list):
        pass

    # TODO: Add something (no need to do it in project 1
