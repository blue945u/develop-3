#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

from cklabidx.analysis.tokenizers.base_tokenizer import Tokenizer


class NGramTokenizer(Tokenizer):
    """
    NGramTokenizer
    """
    def __init__(self, min_gram_size=1, max_gram_size=1):
        self.min_gram_size = 1
        self.max_gram_size = 1
        self.set_gram_size(min_gram_size, max_gram_size)

    def set_gram_size(self, min_gram_size, max_gram_size):
        if min_gram_size > max_gram_size:
            max_gram_size = min_gram_size

        self.min_gram_size = min_gram_size
        self.max_gram_size = max_gram_size

    def generate_tokens(self, text):
        """
        Tokenize text in to N-Gram

        :param text: Input text to be tokenize
        :return: A list of tokens
        """
        tokens = []

        # Preserve whitespace characters
        content = text.strip(' \t\n\r')

        for gram_n in range(self.min_gram_size, self.max_gram_size + 1):
            for index in range(len(content) - gram_n + 1):
                tokens.append(content[index:index + gram_n])

        return tokens
