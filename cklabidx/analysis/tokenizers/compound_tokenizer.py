#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import re

from cklabidx.analysis.tokenizers.base_tokenizer import Tokenizer
from cklabidx.analysis.tokenizers.whitespace_tokenizer import WhitespaceTokenizer


class CompoundTokenizer(Tokenizer):
    """
    CompoundTokenizer
    """

    def generate_tokens(self, text):
        """
        Break input text with whitespace and NGram.

        :param text: Input text to be tokenize
        :return: A list of tokens
        """

        # TODO: Add compound tokenizer (ok
        tokens = []
        w = WhitespaceTokenizer().generate_tokens(text)
        for s in w:
            if s.isdigit():
                tokens.append(s)
            else:
                en_finding = False
                en_word = ""
                for c in s:
                    if not (c.islower() or c.isupper() or c.isdigit()):
                        if en_finding:
                            tokens.append(en_word)
                            en_finding = False
                            en_word = ""
                        tokens.append(c)
                    else:
                        en_word = en_word + c
                        en_finding = True
            if en_finding:
                tokens.append(en_word)
                en_finding = False
                en_word = ""
        return tokens
