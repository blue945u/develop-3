#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#
import nltk

from cklabidx.analysis.filters.base_filter import Filter
from nltk.stem import WordNetLemmatizer


class StemmingFilter(Filter):
    """
    StemmingFilter
    """
    # TODO:  Stemming filter using NLTK (ok

    @staticmethod
    def filter(token_list):
        """
        Filter all tokens to the normal form
        :param token_list: List of Tokens
        :return: lemmatized token list
        """

        tokens = []
        for (vocabulary, tag) in nltk.pos_tag(token_list):
            if "NN" in tag:
                tokens.append(WordNetLemmatizer().lemmatize(vocabulary))
            else:
                tokens.append(WordNetLemmatizer().lemmatize(vocabulary, 'v'))
        return tokens
