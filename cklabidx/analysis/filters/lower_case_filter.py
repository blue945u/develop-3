#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

from cklabidx.analysis.filters.base_filter import Filter


class LowerCaseFilter(Filter):
    """
    LowerCaseFilter
    """

    @staticmethod
    def filter(token_list):
        """
        Filter all tokens to all lower case
        :param token_list: List of Tokens
        :return: Original token list
        """

        tokens = []
        for vocabulary in token_list:
            tokens.append(vocabulary.lower())

        return tokens
